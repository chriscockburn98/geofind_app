//Initialising required variables
var map, map1, map2;
var rand1, rand2;
var dist1, dist2;
var marker;

//Boolean set true on first load
var firstTry = true;

//Random range method to fixed decimal place
function getRandomInRange(from, to, fixed) {
  return (Math.random() * (to - from) + from).toFixed(fixed) * 1;
}

//Produces a random lat and lng JSON object
function getRandomLoc() {
  var jsonObj = {
    lat : getRandomInRange(-85, 85,5),
    lng : getRandomInRange(-175, 175,5)
  }
  return jsonObj;
}

//sets variables to random JSON objects
rand1 = getRandomLoc();
rand2 = getRandomLoc();

//Initializes Google embedded maps API
function initMap() {
    map = new google.maps.Map(document.getElementById('set_location_map'), {
    center: {lat: 55.9533, lng: 3.1883},
    zoom: 2,
    mapTypeId: 'satellite',
    streetViewControl: false,
    fullscreenControl: false,
    mapTypeControl: false
  });

  map1 = new google.maps.Map(document.getElementById('map1'), {
    center: rand1,
    zoom: 5,
    mapTypeId: 'satellite',
    draggable: false,
    zoomControl: false,
    streetViewControl: false,
    fullscreenControl: false,
    mapTypeControl: false
  });

  map2 = new google.maps.Map(document.getElementById('map2'), {
    center: rand2,
    zoom: 5,
    mapTypeId: 'satellite',
    draggable: false,
    zoomControl: false,
    streetViewControl: false,
    fullscreenControl: false,
    mapTypeControl: false
  });

  //Sets marker for the user
  marker = new google.maps.Marker({
    position: {lat: 55.9533, lng: -3.200},
    map: map,
    title: 'Your Location',
    animation: google.maps.Animation.BOUNCE,
    draggable: true
  });      
}

//Gets distance between random map location and user map marker location
function getDistance(randLocation) {
  var lat = marker.getPosition().lat();
  var lng = marker.getPosition().lng();

  console.log("Latitude: " + lat + " " + lng);
  console.log(randLocation + ": " + randLocation.lat + " " + randLocation.lng);

  var a = Math.pow(randLocation.lat - lat,2);
  var b = Math.pow(randLocation.lng - lng,2);

  var d = Math.sqrt(a + b);
  return d;
}

//Once selected will not allow user to play again until refresh
function on(clicked) {
  if(firstTry == false) {
    alert("Sorry you need to start a new game!");
  } else {
    document.getElementById("overlay").style.display = "block";
    dist1 = getDistance(rand1);
    dist2 = getDistance(rand2);

    if(clicked == "option_1") {
      checkResult(dist1, dist2);

    } else if(clicked == "option_2") {
      checkResult(dist2, dist1);

    } else {console.log("Invalid State")};

    console.log("you clicked : " + clicked);
    firstTry = false;
  }
}

//Checks the results
function checkResult(choosenDis, otherDis) {
  if(choosenDis < otherDis) {
    youWin();
  } else {
    youLose();
  }
}

function youWin() {
  console.log("You win");
  document.getElementById('outcome_box').innerHTML = "Congratulations you won!"
}

function youLose() {
  console.log("You lose");
  document.getElementById('outcome_box').innerHTML = "Incorrect, sorry better luck next time!"
}

function off() {
  document.getElementById("overlay").style.display = "none";
}